#include <IRremote.h>
int IRpin=6;                        // pwm pin 
IRrecv IR(IRpin);
decode_results cmd;

int relay=3;


void setup(){
  Serial.begin(9600);
  IR.enableIRIn();
  pinMode(relay,OUTPUT);
  
  }

void loop()
{
  while(IR.decode(&cmd)==0)
  {}
  Serial.println(cmd.value,HEX);
  delay(500);
  IR.resume();
  if (cmd.value == 0xFF30CF||cmd.value == 0x20DF8E71)
  {digitalWrite(relay,HIGH);}
  if (cmd.value == 0xFFA25D||cmd.value == 0x20DF4EB1)
  {digitalWrite(relay,LOW);}
  
  
  }
  
