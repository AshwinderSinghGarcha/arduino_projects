#include<IRremote.h>
int IRpin = 6;
IRrecv IR(IRpin);
decode_results cmd;
String mycom;

int spee=5;
int dir0=3;
int dir5=2;
int x;
int MSpeed=255;
void setup() {
  Serial.begin(9600);
  IR.enableIRIn();

  pinMode(spee,OUTPUT);
  pinMode(dir5,OUTPUT);
  pinMode(dir0,OUTPUT);
  

}

void loop() {
  while(IR.decode(&cmd)==0){}
  //Serial.println(cmd.value,HEX);
  delay(500);
  IR.resume();

  
   
   ///MOTOR CONTROL
   if (cmd.value == 0xFFA25D)
  { mycom = "PWR";
    Serial.println(mycom);}
    if (cmd.value == 0xFFC23D)
  { mycom = "FWB";
    Serial.println(mycom);}
    if (cmd.value == 0xFF22DD)
  { mycom = "REV";
    Serial.println(mycom);}
    if (cmd.value == 0xFF629D)
  { mycom = "VOL+";
    Serial.println(mycom);}
    if (cmd.value == 0xFFA857)
  { mycom = "VOL-";
    Serial.println(mycom);}
    if (cmd.value == 0xFFB04F)
  { mycom = "SET";
    Serial.println(mycom);}
    if (cmd.value == 0xFFE21D)
  { mycom = "STOP";
    Serial.println(mycom);}

if (mycom == "PWR")
    {digitalWrite(dir0,HIGH);
     digitalWrite(dir5,LOW);
     analogWrite(spee,255);
     //Serial.print("fullspeed");
     }

if (mycom == "FWB")
    {digitalWrite(dir0,HIGH);
     digitalWrite(dir5,LOW);
     analogWrite(spee,MSpeed);
    // Serial.print("fullspeed");
    }
 
if (mycom == "REV")
    {spee=-spee;
      digitalWrite(dir0,LOW);
     digitalWrite(dir5,HIGH);
     analogWrite(spee,MSpeed);
     //Serial.print("fullspeed");
     
     }


if (mycom == "VOL+")
    {
     MSpeed=MSpeed+15;
     if (MSpeed>255)
     {MSpeed=255;}
     analogWrite(spee,MSpeed);
     Serial.print(MSpeed);
     }
if (mycom == "VOL-")
    {
    MSpeed=MSpeed-15;
    if (MSpeed<0)
    {MSpeed=0;}
    analogWrite(spee,MSpeed);
    Serial.print(MSpeed);
    }


         
if (mycom == "STOP")
    {digitalWrite(dir5,LOW);
     digitalWrite(dir0,LOW);
     analogWrite(spee,0);}


     }
