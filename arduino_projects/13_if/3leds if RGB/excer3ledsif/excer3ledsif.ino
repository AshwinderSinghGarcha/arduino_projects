int pot    =A0;
int red    =2;
int yellow =3;
int green  =4;
float scale;
int readpot;
int wait = 500;


void setup() {
  Serial.begin(9600);
  pinMode(red,OUTPUT);
  pinMode(yellow,OUTPUT);
  pinMode(green,OUTPUT);
  pinMode(pot,INPUT);
 }
void loop() {
  readpot = analogRead(pot);
  scale = (5.0/1023.0)*readpot;
  Serial.print("VOLTAGE LEVEL IS ");
  Serial.print(scale);
  
  if (scale < 3.0)
    {digitalWrite(green,HIGH);
     digitalWrite(yellow,LOW);
     digitalWrite(red,LOW);
     Serial.println(" NORMAL < 3.0");
    }
  else if (scale <4 && scale >3)
     {digitalWrite(green,LOW);
     digitalWrite(yellow,HIGH);
     digitalWrite(red,LOW);
     Serial.println(" HIGH between3.0-4.0");
     } 
  
  else 
     {digitalWrite(green,LOW);
     digitalWrite(yellow,LOW);
     digitalWrite(red,HIGH);
     Serial.println(" DANGER ZONE > 4.0");
    }
  
  
  delay(wait);
}
