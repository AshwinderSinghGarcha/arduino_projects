int pot = A0;
float scale ;
int adcout;
int wait = 750;
int led = 2;



void setup() {
  Serial.begin(9600);
pinMode(led,OUTPUT);
}

void loop() {
  adcout = analogRead(pot);
  scale = (5.0/1023.0)*adcout;
  Serial.print("VALUE = ");
  Serial.print(scale);

  if(scale <3.55 && scale >3.15){
    digitalWrite(led , HIGH);
    Serial.println("  is in range 3.55 to 3.15 so LED ON");
    }
    else {digitalWrite(led,LOW);
    Serial.println("  is out of  range 3.55 to 3.15 so LED OFF");
    }
  delay(wait);
}
