

int red    = 9;
int yellow = 6;
int green  = 3;
String msg = "WHAT COLOUR LED ";
String msg1 = "RED LED IS ON ";
String msg2 = "YELLOW LED IS ON ";
String msg3 = "GREEN LED IS ON ";
String msg4 = "INVALID INPUT ";
String COLOUR;

void setup() {
 pinMode(red,OUTPUT);
 pinMode(yellow,OUTPUT);
 pinMode(green,OUTPUT);
 Serial.begin(9600);
 }

void loop() {
 Serial.println(msg);
 while(Serial.available()==0){;}

 COLOUR = Serial.readString();
 if (COLOUR == "RED" || COLOUR == "red")
  { digitalWrite(red  ,HIGH);
    digitalWrite(yellow,LOW);
    digitalWrite(green ,LOW);
    Serial.println(msg1);
  }
 if (COLOUR == "YELLOW"|| COLOUR == "yellow")
  { digitalWrite(red  ,LOW);
    digitalWrite(yellow,HIGH);
    digitalWrite(green ,LOW);
    Serial.println(msg2);
  }
 if (COLOUR == "GREEN"|| COLOUR == "green")
  { digitalWrite(red  ,LOW);
    digitalWrite(yellow,LOW);
    digitalWrite(green ,HIGH);
    Serial.println(msg3);
  }
  
 // else 
 // {digitalWrite(red,LOW);
 //  digitalWrite(yellow,LOW);
  // digitalWrite(green,LOW);
  // Serial.println(msg4);
 //  }
}
