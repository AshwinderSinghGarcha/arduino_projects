int red = 3;
int green=5;
int blue=6;
String msg="WHAT COLOUR YOU WANT ";
String colour;
void setup() {
  Serial.begin(9600);
  pinMode(red,OUTPUT);
  pinMode(green,OUTPUT);
  pinMode(blue,OUTPUT);
}

void loop() 
{
 Serial.println(msg);
 while (Serial.available()==0)
 {;}
 colour = Serial.readString();
 if (colour == "red" || colour == "RED" || colour == "r")
 {
   digitalWrite(red,HIGH);
   digitalWrite(blue,LOW);
   digitalWrite(green,LOW);
 }

if (colour == "green" || colour == "GREEN"||colour == "g")
 {
   digitalWrite(red,LOW);
   digitalWrite(blue,LOW);
   digitalWrite(green,HIGH);
 }


 if (colour == "BLUE" || colour == "blue"||colour == "b")
 {
   digitalWrite(red,LOW);
   digitalWrite(blue,HIGH);
   digitalWrite(green,LOW);
 }

if (colour == "off" || colour == "OFF")
 {
   digitalWrite(red,LOW);
   digitalWrite(blue,LOW);
   digitalWrite(green,LOW);
 }

if (colour == "aqua")
 {
    analogWrite(red,255);
    analogWrite(green,165);
    analogWrite(blue,0);
   
 }

}
