int red = 3;
int green=5;
int blue=6;
String msg="ADJUST RGB POT TO MIX COLOURS ";

int r_colour;
int g_colour;
int b_colour;

int r_pot=A0;
int g_pot=A1;
int b_pot=A2;

int r_scale;
int g_scale;
int b_scale;


void setup() {
  Serial.begin(9600);
  pinMode(red,OUTPUT);
  pinMode(green,OUTPUT);
  pinMode(blue,OUTPUT);

   pinMode(r_pot,INPUT);
  pinMode(g_pot,INPUT);
  pinMode(b_pot,INPUT);
  
}

void loop() 
{
 Serial.println(msg);
 while (Serial.available()==0)
 {;}
 
 r_colour = analogRead(r_pot);
 g_colour = analogRead(g_pot);
 b_colour = analogRead(b_pot);

r_scale = r_colour/4.011764706;
g_scale = g_colour/4.011764706;
b_scale = b_colour/4.011764706;


Serial.print(r_scale);
Serial.print(" ; ");
Serial.print(g_scale);
Serial.print(" ; ");
Serial.print(b_scale);
Serial.println("  ");


analogWrite(red,r_scale);
analogWrite(green,g_scale);
analogWrite(blue,b_scale);

 
 delay (500);

}
