#include <DHT.h>

#define type DHT11


int sensePin =2;
DHT HT(sensePin,type);
float humidity;
float tempC;
float tempF;
int settime =500;
int dt = 1000;
void setup() {
  Serial.begin(9600);
 HT.begin();
  delay(settime);

}

void loop() {
 humidity = HT.readHumidity();
 tempC = HT.readTemperature();
 tempF= HT.readTemperature(true);

 
 Serial.print(" humidity: ");
 Serial.print(humidity);
 Serial.print(" ; ");
 Serial.print("TEMPRATURE: ");
 Serial.print(tempC);
 Serial.print("C ");
 Serial.print(tempF);
 Serial.println("F ");

 delay(dt);

}
