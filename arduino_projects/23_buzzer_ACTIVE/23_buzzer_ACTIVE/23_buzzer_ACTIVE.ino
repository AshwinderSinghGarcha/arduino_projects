

int active_buzzer = 8;
int pot = A0;
int potvalue;
int TONE;


void setup() {
  pinMode(pot,INPUT);
  pinMode(active_buzzer,OUTPUT);
  Serial.begin(9600);

}

void loop() {
  potvalue = analogRead(pot);
  TONE = (9940/1023)*potvalue+60;

  Serial.println(TONE);

  digitalWrite(active_buzzer,HIGH);
  delayMicroseconds(TONE);
  digitalWrite(active_buzzer,LOW);
  delayMicroseconds(TONE);
  
  

}
