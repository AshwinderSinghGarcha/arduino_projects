int datapin = 12;
int latchpin=11;
int clkpin=9;
int shiftleft = 0b00000001;            // msb = 0 lsb =1
int dt = 500;


void setup() {
  Serial.begin(9600);
pinMode(datapin,OUTPUT);
pinMode(latchpin,OUTPUT);
pinMode(clkpin,OUTPUT);
}

void loop() {
  
  
  
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,DEC);
  Serial.print("  ");
  Serial.println(shiftleft,BIN);
  delay(dt);
  //shiftleft = shiftleft/2;                         // shift right /2 
  shiftleft = shiftleft*2;                       // shift left *2
  
  
  
  
  /*
   
  shiftleft= 0b10000000;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);

  shiftleft= 0b01000000;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);
  
  shiftleft= 0b00100000;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);
  
  shiftleft= 0b00010000;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);
  
  shiftleft= 0b00001000;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);

  shiftleft= 0b00000100;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);

  shiftleft= 0b00000010;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);
 
  shiftleft= 0b00000001;
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,shiftleft);
  digitalWrite(latchpin,LOW);
  Serial.print(shiftleft,BIN);
  delay(dt);
  */
  } 
