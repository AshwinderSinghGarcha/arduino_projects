int clkpin=9;
int datapin=12;
int latchpin=11;

byte mybyte = 0b10000000;

int dt=500;


void setup() {
  Serial.begin(9600);
  pinMode(clkpin,OUTPUT);
  pinMode(datapin,OUTPUT);
  pinMode(latchpin,OUTPUT);
}

void loop() {

  
  digitalWrite(latchpin,HIGH);
  shiftOut(datapin,clkpin,LSBFIRST,mybyte)  ;
  digitalWrite(latchpin,LOW);

  delay(dt);

 //mybyte = mybyte/128 + mybyte*2;          // left circular shift

  mybyte = mybyte*128 + mybyte/2;
  
}
