# include <Servo.h>;

int black_Servo_pin = 10;
int blue_Servo_pin = 9;

Servo black_servo;
Servo blue_servo;

float serial_black_input ;
float y;
float x;
void setup() {
  black_servo.attach(black_Servo_pin);
  blue_servo.attach(blue_Servo_pin);
  Serial.begin(9600);
}

void loop() {

Serial.print(" ENTER DEGREE from 0 to 180  ");
while (Serial.available()==0){;}
serial_black_input = Serial.parseFloat();

y = - (serial_black_input) +180.0;                  // reverese 180 - 0 to 0 -180 blue
x = (180.0/170.0)*(serial_black_input)+10;         // scaling black 10 -180 

Serial.println(serial_black_input);
if (serial_black_input <= 180 && serial_black_input >= 0){
  black_servo.write(x);
  blue_servo.write(y);
  
  Serial.print(" x ");
  Serial.print(x);
  Serial.print("  ");
  Serial.print(" y ");
  Serial.println(y);}
else {
  Serial.println(" invalid entry ");}
}
