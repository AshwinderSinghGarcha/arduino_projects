#include <Stepper.h>

int stepsPerRevolution =200 ;
int motSpeed = 1;
//Stepper myStepper(stepsPerRevolution, 8,10,9,11);
Stepper myStepper(stepsPerRevolution, 9,10,11,12);
int dt = 2000;




void setup() {
 Serial.begin(9600);
 myStepper.setSpeed(motSpeed);

}

void loop() {
  myStepper.step(stepsPerRevolution);
  Serial.print(stepsPerRevolution);
  delay (dt);
  myStepper.step(-stepsPerRevolution);
  delay (dt);
   Serial.println(-stepsPerRevolution);
}
