#include <Stepper.h>
int motspeed = 10 ;
int stepsPerRevolution = 2048;
int dt =500;
Stepper mystepper(stepsPerRevolution, 8,10,9,11);

void setup() 
{ 
  Serial.begin(9600);
  mystepper.setSpeed(motspeed);
}

void loop() 
{
  mystepper.step(512);
  delay (dt);
  mystepper.step(-512);
  delay (dt);
}
