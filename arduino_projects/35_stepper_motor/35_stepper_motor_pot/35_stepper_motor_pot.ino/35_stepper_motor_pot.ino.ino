#include<Stepper.h>

int in1 = 2;
int in2 = 3;
int in3 = 4;
int in4 = 5;

int SPR = 2048;
int speed =10;
int dt = 500;

Stepper mot (SPR , in1,in2,in3,in4);


int pot = A0;
int potreading;
int scale;

void setup() {
  Serial.begin(9600);
  mot.setSpeed(speed);
  pinMode(pot,INPUT);
}

void loop() {

  potreading = analogRead(pot);
  scale =( potreading +1)*2; 
  Serial.println(scale);
  
  
  mot.step(scale);
  delay(dt);
  mot.step(-scale);
  delay(dt);

}
