#include <Stepper.h>

int speed = 10;
int stepsPerRevolution =2048;

int in1 = 2;
int in2 = 3;
int in3 = 4;
int in4 = 5;

Stepper mot (stepsPerRevolution, in1,in2,in3,in4);


int dt =500;


void setup() {
  Serial.begin(9600);
  mot.setSpeed(speed);

}

void loop() {
  mot.step(512);
  delay(dt);
  mot.step(-512);
  delay(dt);

}
