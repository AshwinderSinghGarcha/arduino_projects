int light = A3;
int lightvalue;
int red=8;
int green=9;
int buzzer = 10;
int TONE;
void setup() {
  pinMode(light,INPUT);
  pinMode(red,OUTPUT);
  pinMode(green,OUTPUT);
  pinMode(buzzer,OUTPUT);
  Serial.begin(9600);

}

void loop() {
  lightvalue = analogRead(light);
  Serial.println(lightvalue);


  // buzzer scaling 50 - 380 photorestor to 60 -10000 microseconds
  TONE = (9940/330)*lightvalue+10;
  Serial.println(TONE);
  

// buzzer sound
digitalWrite(buzzer,HIGH);
delayMicroseconds(TONE);
digitalWrite(buzzer,LOW);
delayMicroseconds(TONE);

/// leds on off
if (lightvalue >=300 )
{ digitalWrite(green,HIGH);
  digitalWrite(red,LOW);
}
if (lightvalue <= 100 )
{
  digitalWrite(green,LOW);
  digitalWrite(red,HIGH);
  }
}
