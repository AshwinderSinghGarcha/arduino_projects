#include<LiquidCrystal.h>
int pb =5;
int led_bklt=4;
int pb_present;
int rs=7;int en=8;int d4 = 9;int d5=10; int d6=11; int d7=12;
LiquidCrystal LCD(rs,en,d4,d5,d6,d7);
int triggerpin = 2;
int echopin=3;
float pingTraveldistance;

float distance;
float osciloscope;


int bucket [6];
float avg;
void setup() {
 Serial.begin(9600);
 pinMode(pb,INPUT);
 digitalWrite(pb,HIGH);
 pinMode(led_bklt,OUTPUT);
 pinMode(triggerpin,OUTPUT);
 pinMode(echopin,INPUT);
 LCD.begin(16,2);
}

void loop() { 
 pb_present = digitalRead(pb);
  if (pb_present == 0)
  {digitalWrite(led_bklt,HIGH);
  digitalWrite(triggerpin,LOW);

  delayMicroseconds(10);
  digitalWrite(triggerpin,HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerpin,LOW);
  osciloscope=pulseIn(echopin,HIGH);
  
  delay(25);
  
//pingTraveldistance = (765.0*5280*12*osciloscope)/(3600.0*1000000.0);
  //distance = pingTraveldistance/2;
 distance= (osciloscope *.0067 );

 for (int k=0 ;k<5;k++ )
 { bucket[k]=distance;
 avg = (bucket[0]+bucket[1]+bucket[2]+bucket[3]+bucket[4]+bucket[5])/6;
 
  }
  Serial.print("distance ");
// Serial.print(0);
 //Serial.print(",");
 Serial.println(distance);
 Serial.print(" avg ");
 Serial.print(avg);
 //Serial.print(",");
 //Serial.println(10);
 //Serial.print(" echo delay time ");
 //Serial.println(osciloscope);
  //delay(100);

   LCD.clear();
  LCD.setCursor(0,0);
  LCD.print("Target Distance ");
  LCD.setCursor(0,2);
  LCD.print(distance);
  LCD.setCursor(5,2);
  LCD.print(" Inches");
  delay(500);
 
  }
  else {LCD.clear();
  digitalWrite(led_bklt,LOW);}
}
